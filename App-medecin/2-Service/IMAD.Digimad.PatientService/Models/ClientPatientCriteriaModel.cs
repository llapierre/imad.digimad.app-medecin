﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.PatientService.Models
{
    public class ClientPatientCriteriaModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AVS { get; set; }
        public DateTime? BirthDate { get; set; }
        public string NPA { get; set; }
        public string WithAwaitingTask { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
