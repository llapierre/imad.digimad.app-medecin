﻿using IMAD.Digimad.Data.Entities;
using IMAD.Digimad.PatientService.Models;
using IMAD.Digimad.Service.Models;

namespace IMAD.Digimad.PatientService.Models
{
    public static class ClientPatientMapper
    {
        #region Entity to Model
        public static ClientPatientModel Map(this ClientPatientModel model, ClientPatient entity)
        {
            ((ClientPatientBaseModel)model).Map(entity);

            return model;
        }
        #endregion Entity to Model

        #region Model to Entity
        #endregion Model to Entity
    }
}
