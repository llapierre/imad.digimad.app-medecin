﻿using System.Collections.Generic;
using System.Linq;
using IMAD.Digimad.Data.Repositories;
using IMAD.Digimad.PatientService.Models;
using IMAD.Digimad.Service.Experts;

namespace IMAD.Digimad.PatientService.Experts
{
    public class PatientExpert : BaseExpert
    {
        private readonly ClientPatientRepository _clientPatientRepository;

        public PatientExpert(ClientPatientRepository clientPatientRepository)
        {
            _clientPatientRepository = clientPatientRepository;
        }

        public IEnumerable<ClientPatientModel> GetPatients(ClientPatientCriteriaModel filter)
        {
            var request = _clientPatientRepository.Fetch();
            if (filter != null)
            {
                if (!string.IsNullOrEmpty(filter.AVS))
                    request = request.Where(item => item.Personne.AVS.Contains(filter.AVS));

                if (filter.BirthDate.HasValue)
                    request = request.Where(item => item.Personne.DateNaissance == filter.BirthDate);

                if (!string.IsNullOrEmpty(filter.FirstName))
                    request = request.Where(item => item.Personne.Prenom.Contains(filter.FirstName));

                if (!string.IsNullOrEmpty(filter.LastName))
                    request = request.Where(item => item.Personne.Nom.Contains(filter.LastName));

                if (!string.IsNullOrEmpty(filter.NPA))
                    request = request.Where(item => item.Personne.PersonneAdresses.Any(item2 => item2.CodePostal == filter.NPA));

                if (!string.IsNullOrEmpty(filter.Phone))
                    request = request.Where(item => item.Personne.Contacts.Any(item2 => item2.Valeur.Contains(filter.Phone)));

                //if (!string.IsNullOrEmpty(filter.WithAwaitingTask))
                //if (!string.IsNullOrEmpty(filter.Email))

            }

            foreach (var model in request)
                yield return new ClientPatientModel().Map(model);
        }
    }
}
