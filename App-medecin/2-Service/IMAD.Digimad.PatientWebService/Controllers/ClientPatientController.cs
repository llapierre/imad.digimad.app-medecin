﻿using IMAD.Digimad.Data.Repositories;
using IMAD.Digimad.PatientService.Experts;
using IMAD.Digimad.PatientService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PatientWebService.Controllers
{
    public class ClientPatientController : BaseController
    {
        private readonly ClientPatientRepository _clientPatientRepository;
        private readonly PatientExpert _patientManager;

        public ClientPatientController()
        {
            _clientPatientRepository = new ClientPatientRepository();
            _patientManager = new PatientExpert(_clientPatientRepository);
        }

        public List<ClientPatientModel> GetAll()
        {
            return  _patientManager.GetPatients(new ClientPatientCriteriaModel())?.ToList();
        }

        public async Task<List<ClientPatientModel>> GetAllAsync()
        {
            return await Task.FromResult(GetAll());
        }
    }
}
