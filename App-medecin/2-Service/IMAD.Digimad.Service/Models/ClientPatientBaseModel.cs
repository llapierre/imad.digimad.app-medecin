﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Service.Models
{
    public class ClientPatientBaseModel : PersonneBaseModel
    {
        public string OrganisationCode { get; set; }
    }
}
