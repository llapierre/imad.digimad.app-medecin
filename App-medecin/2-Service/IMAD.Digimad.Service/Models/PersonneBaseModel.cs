﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Service.Models
{
    public class PersonneBaseModel
    {
        public long PersonneID { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public string NomJeuneFille { get; set; }
        public string Sexe { get; set; }
        public DateTime? DateNaissance { get; set; }
        public DateTime? DateDeces { get; set; }
        public string AVS { get; set; }
    }
}
