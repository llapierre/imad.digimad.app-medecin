﻿using IMAD.Digimad.Data.Entities;

namespace IMAD.Digimad.Service.Models
{
    public static class PersonneBaseMapper
    {
        #region Entity to Model
        public static PersonneBaseModel Map(this PersonneBaseModel model, Personne entity)
        {
            model.AVS = entity.AVS;
            model.DateDeces = entity.DateDeces;
            model.DateNaissance = entity.DateNaissance;
            model.Nom = entity.Nom;
            model.NomJeuneFille = entity.NomJeuneFille;
            model.PersonneID = entity.PersonneID;
            model.Prenom = entity.Prenom;
            model.Sexe = entity.Sexe;

            return model;
        }
        #endregion Entity to Model

        #region Model to Entity
        #endregion Model to Entity
    }
}
