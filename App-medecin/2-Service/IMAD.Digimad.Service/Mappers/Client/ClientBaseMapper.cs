﻿using IMAD.Digimad.Data.Entities;
using IMAD.Digimad.Service.Models;

namespace IMAD.Digimad.Service.Models
{
    public static class ClientBaseMapper
    {
        #region Entity to Model
        public static ClientPatientBaseModel Map(this ClientPatientBaseModel model, ClientPatient entity)
        {
            model.Map(entity.Personne);

            model.OrganisationCode = entity.OrganisationCode;
            return model;
        }
        #endregion Entity to Model

        #region Model to Entity
        #endregion Model to Entity
    }
}
