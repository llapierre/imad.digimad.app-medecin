﻿using System.Collections.Generic;
using IMAD.Digimad.PatientService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PatientWebService.Controllers;

namespace IMAD.Digimad.PatientWebService.Tests
{
    [TestClass]
    public class TestClientPatientController
    {
        [TestMethod]
        public void GetAll_ShouldReturnAllClients()
        {
            var controller = new ClientPatientController();

            var result = controller.GetAll();

            Assert.AreEqual(0, result?.Count ?? 0);
        }

        [TestMethod]
        public async void GetAllAsync_ShouldReturnAllClients()
        {
            var controller = new ClientPatientController();
            var result = await controller.GetAllAsync() as List<ClientPatientModel>;

            Assert.AreEqual(result.Count, 0);
        }
    }
}
