﻿using System.Linq;
using System.Web.Mvc;
using IMAD.Digimad.AppMedecin.ViewModels;
using IMAD.Digimad.Data.Repositories;
using IMAD.Digimad.Data.UnitOfWork;
using IMAD.Digimad.PatientService.Experts;

namespace IMAD.Digimad.AppMedecin.Controllers
{
    public class PatientController : Controller
    {
        private readonly ClientPatientRepository _clientPatientRepository;
        private readonly PatientExpert _patientManager;

        public PatientController()
        {
            UnitOfWork uow = new UnitOfWork();
            _clientPatientRepository = new ClientPatientRepository(uow);
            _patientManager = new PatientExpert(_clientPatientRepository);
        }

        // GET: Patient
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetPatients(PatientFilterViewModel patientfilter)
        {
            DataTableParamViewModel param = new DataTableParamViewModel();
            TryUpdateModel(param);
            var result = _patientManager.GetPatients(new PatientService.Models.ClientPatientCriteriaModel().Map(patientfilter))
                .Select(item => new PatientViewModel().Map(item))
                .ToList();
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = result.Count(),
                iTotalDisplayRecords = result.Count(),
                aaData = result
            },
                JsonRequestBehavior.AllowGet);
        }
    }
}