﻿using System.Web;
using System.Web.Mvc;

namespace IMAD.Digimad.AppMedecin
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
