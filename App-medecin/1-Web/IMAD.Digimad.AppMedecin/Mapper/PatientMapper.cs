﻿using IMAD.Digimad.PatientService.Models;

namespace IMAD.Digimad.AppMedecin.ViewModels
{
    public static class PatientMapper
    {
        #region ViewModel to Model
        public static ClientPatientCriteriaModel Map(this ClientPatientCriteriaModel model, PatientFilterViewModel viewmodel)
        {
            model.AVS = viewmodel.patientfilterAVS;
            model.BirthDate = viewmodel.patientfilterBirthDate;
            model.Email = viewmodel.patientfilterEmail;
            model.FirstName = viewmodel.patientfilterFirstName;
            model.LastName = viewmodel.patientfilterLastName;
            model.NPA = viewmodel.patientfilterNPA;
            model.Phone = viewmodel.patientfilterPhone;
            model.WithAwaitingTask = viewmodel.patientfilterWithAwaitingTask;

            return model;
        }
        #endregion ViewModel to Model

        #region Model to ViewModel
        public static PatientViewModel Map(this PatientViewModel viewModel, ClientPatientModel model)
        {
            viewModel.Id = model.PersonneID;
            viewModel.FirstName = model.Prenom;
            viewModel.LastName = model.Nom;

            return viewModel;
        }
        #endregion Model to ViewModel
    }
}