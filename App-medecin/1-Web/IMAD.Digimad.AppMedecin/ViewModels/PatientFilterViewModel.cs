﻿using System;

namespace IMAD.Digimad.AppMedecin.ViewModels
{
    public class PatientFilterViewModel
    {
        public string patientfilterFirstName { get; set; }
        public string patientfilterLastName { get; set; }
        public string patientfilterAVS { get; set; }
        public DateTime? patientfilterBirthDate { get; set; }
        public string patientfilterNPA { get; set; }
        public string patientfilterWithAwaitingTask { get; set; }
        public string patientfilterPhone { get; set; }
        public string patientfilterEmail { get; set; }

    }
}