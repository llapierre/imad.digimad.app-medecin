﻿using System.ComponentModel.DataAnnotations;

namespace IMAD.Digimad.AppMedecin.ViewModels
{
    public class PatientViewModel
    {
        public long Id { get; set; }

        [Display(Name = "Prénom")]
        public string FirstName { get; set; }
        [Display(Name = "Nom")]
        public string LastName { get; set; }
        [Display(Name = "AVS")]
        public string AVS { get; set; }
        [Display(Name = "Date de naissance")]
        public string BirthDate { get; set; }
        [Display(Name = "NPA")]
        public string NPA { get; set; }
        [Display(Name = "Avec des tâches en attente")]
        public string WithAwaitingTask { get; set; }
        [Display(Name = "Téléphone")]
        public string Phone { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}