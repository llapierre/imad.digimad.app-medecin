﻿using IMAD.Digimad.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data
{
    public class DataContext : DbContext
    {
        public DataContext() : base("DataContext")
        {
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }

        public static DataContext Create()
        {
            return new DataContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region schema rp

            #region client
            modelBuilder.Entity<ClientPatient>()
                .Property(e => e.Etat)
                .HasColumnName(ClientPatient.COLUMN_PREFIX +"_"+ ClientPatient.COLUMN_NAME_ETAT);
            modelBuilder.Entity<ClientPatient>()
                .Property(e => e.DateDebut)
                .HasColumnName(ClientPatient.COLUMN_PREFIX + "_" + ClientPatient.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<ClientPatient>()
                .Property(e => e.DateFin)
                .HasColumnName(ClientPatient.COLUMN_PREFIX + "_" + ClientPatient.COLUMN_NAME_DATEFIN);

            modelBuilder.Entity<ClientProcheAidant>()
                .Property(e => e.Etat)
                .HasColumnName(ClientProcheAidant.COLUMN_PREFIX + "_" + ClientProcheAidant.COLUMN_NAME_ETAT);
            modelBuilder.Entity<ClientProcheAidant>()
                .Property(e => e.DateDebut)
                .HasColumnName(ClientProcheAidant.COLUMN_PREFIX + "_" + ClientProcheAidant.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<ClientProcheAidant>()
                .Property(e => e.DateFin)
                .HasColumnName(ClientProcheAidant.COLUMN_PREFIX + "_" + ClientProcheAidant.COLUMN_NAME_DATEFIN);
            #endregion client
                        
            #region employe
            modelBuilder.Entity<Employe>()
                .Property(e => e.Etat)
                .HasColumnName(Employe.COLUMN_PREFIX + "_" + Employe.COLUMN_NAME_ETAT);
            modelBuilder.Entity<Employe>()
                .Property(e => e.DateDebut)
                .HasColumnName(Employe.COLUMN_PREFIX + "_" + Employe.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<Employe>()
                .Property(e => e.DateFin)
                .HasColumnName(Employe.COLUMN_PREFIX + "_" + Employe.COLUMN_NAME_DATEFIN);
            modelBuilder.Entity<Employe>()
                .Property(e => e.Matricule)
                .IsUnicode(false);
            modelBuilder.Entity<Employe>()
                .Property(e => e.TauxActivite)
                .HasPrecision(5, 2);
            modelBuilder.Entity<Employe>()
                .HasMany(e => e.EmployeFonctions)
                .WithRequired(e => e.Employe)
                .HasForeignKey(e => e.EmployeID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EmployeFonction>()
                .Property(e => e.Etat)
                .HasColumnName(EmployeFonction.COLUMN_PREFIX + "_" + EmployeFonction.COLUMN_NAME_ETAT);
            modelBuilder.Entity<EmployeFonction>()
                .Property(e => e.DateDebut)
                .HasColumnName(EmployeFonction.COLUMN_PREFIX + "_" + EmployeFonction.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<EmployeFonction>()
                .Property(e => e.DateFin)
                .HasColumnName(EmployeFonction.COLUMN_PREFIX + "_" + EmployeFonction.COLUMN_NAME_DATEFIN);


            modelBuilder.Entity<Fonction>()
                .HasMany(e => e.EmployeFonctions)
                .WithRequired(e => e.Fonction)
                .HasForeignKey(e => e.FonctionCode)
                .WillCascadeOnDelete(false);

            #endregion employe

            #region geo
            modelBuilder.Entity<AdresseStandard>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<AdresseStandard>()
                .Property(e => e.Rue)
                .IsUnicode(false);
            modelBuilder.Entity<AdresseStandard>()
                .HasMany(e => e.PersonneAdresses)
                .WithOptional(e => e.AdresseStandard)
                .HasForeignKey(e => e.AdresseStandardID);

            modelBuilder.Entity<BureauPostal>()
                .Property(e => e.PaysCode)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<BureauPostal>()
                .Property(e => e.CodePostal)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<BureauPostal>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<BureauPostal>()
                .Property(e => e.Nom)
                .IsUnicode(false);
            modelBuilder.Entity<BureauPostal>()
                .Property(e => e.Commune)
                .IsUnicode(false);
            modelBuilder.Entity<BureauPostal>()
                .Property(e => e.Canton)
                .IsUnicode(false);
            modelBuilder.Entity<BureauPostal>()
                .HasMany(e => e.AdresseStandards)
                .WithRequired(e => e.BureauPostal)
                .HasForeignKey(e => e.BureauPostalID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .Property(e => e.LanguageCode)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<Language>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<Language>()
                .HasMany(e => e.Personnes)
                .WithOptional(e => e.Language)
                .HasForeignKey(e => e.LanguageCode);

            modelBuilder.Entity<Pays>()
                .Property(e => e.PaysCode)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<Pays>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<Pays>()
                .Property(e => e.ZoneCode)
                .IsUnicode(false);
            modelBuilder.Entity<Pays>()
                .HasMany(e => e.BureauPostals)
                .WithRequired(e => e.Pays)
                .HasForeignKey(e => e.PaysCode)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Pays>()
                .HasMany(e => e.Personnes)
                .WithOptional(e => e.PaysNationalite)
                .HasForeignKey(e => e.PaysNationaliteCode);

            modelBuilder.Entity<Zone>()
                .Property(e => e.ZoneCode)
                .IsUnicode(false);
            modelBuilder.Entity<Zone>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<Zone>()
                .Property(e => e.Nom)
                .IsUnicode(false);
            modelBuilder.Entity<Zone>()
                .Property(e => e.ZoneTypeCode)
                .IsUnicode(false);
            modelBuilder.Entity<Zone>()
                .Property(e => e.ZoneParentCode)
                .IsUnicode(false);
            modelBuilder.Entity<Zone>()
                .HasMany(e => e.Pays)
                .WithOptional(e => e.Zone)
                .HasForeignKey(e => e.ZoneCode);
            modelBuilder.Entity<Zone>()
                .HasMany(e => e.ZoneEnfants)
                .WithOptional(e => e.ZoneParent)
                .HasForeignKey(e => e.ZoneParentCode);

            modelBuilder.Entity<ZoneType>()
                .Property(e => e.ZoneTypeCode)
                .IsUnicode(false);
            modelBuilder.Entity<ZoneType>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<ZoneType>()
                .Property(e => e.ZoneTypeParentCode)
                .IsUnicode(false);
            modelBuilder.Entity<ZoneType>()
                .HasMany(e => e.Zones)
                .WithRequired(e => e.ZoneType)
                .HasForeignKey(e => e.ZoneTypeCode)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<ZoneType>()
                .HasMany(e => e.ZoneTypeEnfants)
                .WithOptional(e => e.ZoneTypeParent)
                .HasForeignKey(e => e.ZoneTypeParentCode);

            #endregion geo

            #region org
            modelBuilder.Entity<Organisation>()
                .Property(e => e.Etat)
                .HasColumnName(Organisation.COLUMN_PREFIX + "_" + Organisation.COLUMN_NAME_ETAT);
            modelBuilder.Entity<Organisation>()
                .Property(e => e.DateDebut)
                .HasColumnName(Organisation.COLUMN_PREFIX + "_" + Organisation.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<Organisation>()
                .Property(e => e.DateFin)
                .HasColumnName(Organisation.COLUMN_PREFIX + "_" + Organisation.COLUMN_NAME_DATEFIN);
            modelBuilder.Entity<Organisation>()
                .Property(e => e.Discriminant)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Organisation>()
                .Property(e => e.Nom)
                .IsUnicode(false);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.ClientPatients)
                .WithOptional(e => e.Organisation)
                .HasForeignKey(e => e.OrganisationCode);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.EmployeFonctions)
                .WithRequired(e => e.Organisation)
                .HasForeignKey(e => e.OrganisationCode)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.OrganisationParents)
                .WithRequired(e => e.OrganisationEnfant)
                .HasForeignKey(e => e.OrganisationEnfantID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.OrganisationEnfants)
                .WithOptional(e => e.OrganisationParent)
                .HasForeignKey(e => e.OrganisationParentID);


            modelBuilder.Entity<OrganisationHierarchy>()
                .Property(e => e.Etat)
                .HasColumnName(OrganisationHierarchy.COLUMN_PREFIX + "_" + OrganisationHierarchy.COLUMN_NAME_ETAT);
            modelBuilder.Entity<OrganisationHierarchy>()
                .Property(e => e.DateDebut)
                .HasColumnName(OrganisationHierarchy.COLUMN_PREFIX + "_" + OrganisationHierarchy.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<OrganisationHierarchy>()
                .Property(e => e.DateFin)
                .HasColumnName(OrganisationHierarchy.COLUMN_PREFIX + "_" + OrganisationHierarchy.COLUMN_NAME_DATEFIN);


            modelBuilder.Entity<OrganisationType>()
                .Property(e => e.Etat)
                .HasColumnName(OrganisationType.COLUMN_PREFIX + "_" + OrganisationType.COLUMN_NAME_ETAT);
            modelBuilder.Entity<OrganisationType>()
                .Property(e => e.DateDebut)
                .HasColumnName(OrganisationType.COLUMN_PREFIX + "_" + OrganisationType.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<OrganisationType>()
                .Property(e => e.DateFin)
                .HasColumnName(OrganisationType.COLUMN_PREFIX + "_" + OrganisationType.COLUMN_NAME_DATEFIN);
            modelBuilder.Entity<OrganisationType>()
                .HasMany(e => e.Organisations)
                .WithRequired(e => e.OrganisationType)
                .HasForeignKey(e => e.OrganisationTypeCode)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<PersonneMorale>()
                .Property(e => e.Etat)
                .HasColumnName(PersonneMorale.COLUMN_PREFIX + "_" + PersonneMorale.COLUMN_NAME_ETAT);
            modelBuilder.Entity<PersonneMorale>()
                .Property(e => e.DateDebut)
                .HasColumnName(PersonneMorale.COLUMN_PREFIX + "_" + PersonneMorale.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<PersonneMorale>()
                .Property(e => e.DateFin)
                .HasColumnName(PersonneMorale.COLUMN_PREFIX + "_" + PersonneMorale.COLUMN_NAME_DATEFIN);
            modelBuilder.Entity<PersonneMorale>()
                .Property(e => e.Libelle)
                .IsUnicode(false);
            modelBuilder.Entity<PersonneMorale>()
                .HasMany(e => e.PartenaireMedecins)
                .WithRequired(e => e.PersonneMorale)
                .HasForeignKey(e => e.PersonneMoraleID)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<PersonneMorale>()
                .HasMany(e => e.PartenairePharmaciens)
                .WithRequired(e => e.PersonneMorale)
                .HasForeignKey(e => e.PersonneMoraleID)
                .WillCascadeOnDelete(false);

            #endregion org

            #region partenariat

            modelBuilder.Entity<PartenaireMedecin>()
                .Property(e => e.Etat)
                .HasColumnName(PartenaireMedecin.COLUMN_PREFIX + "_" + PartenaireMedecin.COLUMN_NAME_ETAT);
            modelBuilder.Entity<PartenaireMedecin>()
                .Property(e => e.DateDebut)
                .HasColumnName(PartenaireMedecin.COLUMN_PREFIX + "_" + PartenaireMedecin.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<PartenaireMedecin>()
                .Property(e => e.DateFin)
                .HasColumnName(PartenaireMedecin.COLUMN_PREFIX + "_" + PartenaireMedecin.COLUMN_NAME_DATEFIN);


            modelBuilder.Entity<PartenairePharmacien>()
               .Property(e => e.Etat)
               .HasColumnName(PartenairePharmacien.COLUMN_PREFIX + "_" + PartenairePharmacien.COLUMN_NAME_ETAT);
            modelBuilder.Entity<PartenairePharmacien>()
                .Property(e => e.DateDebut)
                .HasColumnName(PartenairePharmacien.COLUMN_PREFIX + "_" + PartenairePharmacien.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<PartenairePharmacien>()
                .Property(e => e.DateFin)
                .HasColumnName(PartenairePharmacien.COLUMN_PREFIX + "_" + PartenairePharmacien.COLUMN_NAME_DATEFIN);
            #endregion partenariat

            #region Personne

            modelBuilder.Entity<PersonneAdresse>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);
                        
            modelBuilder.Entity<PersonneAdresseType>()
                .HasMany(e => e.PersonneAdresses)
                .WithRequired(e => e.PersonneAdresseType)
                .HasForeignKey(e => e.PersonneAdresseTypeCode)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Contact>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);            

            modelBuilder.Entity<ContactType>()
                .HasMany(e => e.Contacts)
                .WithRequired(e => e.ContactType)
                .HasForeignKey(e => e.ContactTypeCode)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ContactType>()
                .HasMany(e => e.ContactTypeEnfants)
                .WithOptional(e => e.ContactTypeParent)
                .HasForeignKey(e => e.ContactTypeParentCode);

            modelBuilder.Entity<Personne>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);            
            modelBuilder.Entity<Personne>()
                .HasMany(e => e.ClientPatients)
                .WithRequired(e => e.Personne)
                .HasForeignKey(e => e.Personne_PersonneID)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Personne>()
                .HasMany(e => e.ClientProcheAidants)
                .WithRequired(e => e.Personne)
                .HasForeignKey(e => e.PersonneID)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Personne>()
                .HasMany(e => e.Employes)
                .WithRequired(e => e.Personne)
                .HasForeignKey(e => e.PersonneID)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Personne>()
                .HasMany(e => e.PartenaireMedecins)
                .WithRequired(e => e.Personne)
                .HasForeignKey(e => e.PersonneID)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Personne>()
                .HasMany(e => e.PartenairePharmaciens)
                .WithRequired(e => e.Personne)
                .HasForeignKey(e => e.PersonneID)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Personne>()
                .HasMany(e => e.PersonneAdresses)
                .WithRequired(e => e.Personne)
                .HasForeignKey(e => e.PersonneID)
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Personne>()
                .HasMany(e => e.Contacts)
                .WithRequired(e => e.Personne)
                .HasForeignKey(e => e.PersonneID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonneEtatCivil>()
                .Property(e => e.PersonneEtatCivilCode)
                .IsUnicode(false);
            modelBuilder.Entity<PersonneEtatCivil>()
                .Property(e => e.Etat)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<PersonneEtatCivil>()
                .HasMany(e => e.Personnes)
                .WithOptional(e => e.PersonneEtatCivil)
                .HasForeignKey(e => e.PersonneEtatCivilCode);

            modelBuilder.Entity<PersonneTitre>()
                .Property(e => e.PersonneTitreCode)
                .IsUnicode(false);
            modelBuilder.Entity<PersonneTitre>()
                .HasMany(e => e.Personnes)
                .WithOptional(e => e.PersonneTitre)
                .HasForeignKey(e => e.PersonneTitreCode);

            #endregion Personne

            #region Prestation           
            modelBuilder.Entity<Mandat>()
                .Property(e => e.Etat)
                .HasColumnName(Mandat.COLUMN_PREFIX + "_" + Mandat.COLUMN_NAME_ETAT);
            modelBuilder.Entity<Mandat>()
                .Property(e => e.DateDebut)
                .HasColumnName(Mandat.COLUMN_PREFIX + "_" + Mandat.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<Mandat>()
                .Property(e => e.DateFin)
                .HasColumnName(Mandat.COLUMN_PREFIX + "_" + Mandat.COLUMN_NAME_DATEFIN);
            modelBuilder.Entity<Mandat>()
                .Property(e => e.Type)
                .IsFixedLength()
                .IsUnicode(false);
            modelBuilder.Entity<Mandat>()
                .HasMany(e => e.Referants)
                .WithRequired(e => e.Mandat)
                .HasForeignKey(e => e.MandatID)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<Referant>()
                .Property(e => e.Etat)
                .HasColumnName(Referant.COLUMN_PREFIX + "_" + Referant.COLUMN_NAME_ETAT);
            modelBuilder.Entity<Referant>()
                .Property(e => e.DateDebut)
                .HasColumnName(Referant.COLUMN_PREFIX + "_" + Referant.COLUMN_NAME_DATEDEBUT);
            modelBuilder.Entity<Referant>()
                .Property(e => e.DateFin)
                .HasColumnName(Referant.COLUMN_PREFIX + "_" + Referant.COLUMN_NAME_DATEFIN);

            modelBuilder.Entity<ReferantRole>()
                .HasMany(e => e.Referants)
                .WithRequired(e => e.ReferantRole)
                .HasForeignKey(e => e.ReferantRoleCode)
                .WillCascadeOnDelete(false);
            #endregion Prestation
            #endregion schema rp
        }

        #region Properties
        public virtual DbSet<ClientPatient> ClientPatients { get; set; }
        public virtual DbSet<Employe> Employes { get; set; }
        public virtual DbSet<EmployeFonction> EmployeFonctions { get; set; }
        public virtual DbSet<Fonction> Fonctions { get; set; }
        public virtual DbSet<Organisation> Organisations { get; set; }
        public virtual DbSet<OrganisationHierarchy> OrganisationHierarchies { get; set; }
        public virtual DbSet<OrganisationType> OrganisationTypes { get; set; }

        public virtual DbSet<ClientProcheAidant> ClientProcheAidants { get; set; }

        public virtual DbSet<AdresseStandard> AdresseStandards { get; set; }
        public virtual DbSet<BureauPostal> BureauPostals { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<Pays> Pays { get; set; }
        public virtual DbSet<Zone> Zones { get; set; }
        public virtual DbSet<ZoneType> ZoneTypes { get; set; }

        public virtual DbSet<PersonneMorale> PersonneMorales { get; set; }
        public virtual DbSet<PartenaireMedecin> PartenaireMedecins { get; set; }
        public virtual DbSet<PartenairePharmacien> PartenairePharmaciens { get; set; }
        public virtual DbSet<PersonneAdresse> PersonneAdresses { get; set; }
        public virtual DbSet<PersonneAdresseType> PersonneAdresseTypes { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<ContactType> ContactType { get; set; }
        public virtual DbSet<Personne> Personne { get; set; }
        public virtual DbSet<PersonneEtatCivil> PersonneEtatCivils { get; set; }
        public virtual DbSet<PersonneTitre> PersonneTitres { get; set; }
        public virtual DbSet<Mandat> Mandats { get; set; }
        public virtual DbSet<Referant> Referants { get; set; }
        public virtual DbSet<ReferantRole> ReferantRoles { get; set; }
        #endregion
    }
}
