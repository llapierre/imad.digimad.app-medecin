﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        #region constructor
        public UnitOfWork()
        {
            Context = new DataContext();
        }

        public UnitOfWork(DataContext pContext)
        {
            this.Context = pContext;
        }
        #endregion constructor

        #region property
        public DbContext Context { get; set; }
        protected DbContextTransaction _transaction;
        #endregion property

        #region Method
        public void StartTransaction()
        {
            _transaction = Context.Database.BeginTransaction();
        }
        public void CommitTransaction()
        {
            _transaction.Commit();
        }
        public void RollbackTransaction()
        {
            _transaction.Rollback();
        }
        public void Save()
        {
            Context.SaveChanges();
        }

        public DbRawSqlQuery<T> SQLQuery<T>(string sql, params object[] parameters)
        {
            return Context.Database.SqlQuery<T>(sql, parameters);
        }

        public void Dispose()
        {
            if (Context != null)
                Context.Dispose();
            if (_transaction != null)
                _transaction.Dispose();
        }
        #endregion Method
    }
}
