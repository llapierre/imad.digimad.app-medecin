﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("PAR_PHARMACIEN", Schema = "rp")]
    public class PartenairePharmacien : HistoryEntity
    {
        public const String COLUMN_PREFIX = "RPE";
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long PartenairePharmacienID { get; set; }

        #region links
        [Column(COLUMN_PREFIX+"_FK_"+Personne.COLUMN_PREFIX+"_ID")]
        [ForeignKey("Personne")]
        public long PersonneID { get; set; }
        public virtual Personne Personne { get; set; }
        [Column(COLUMN_PREFIX + "_FK_" + PersonneMorale.COLUMN_PREFIX + "_ID")]
        [ForeignKey("PersonneMorale")]
        public long PersonneMoraleID { get; set; }
        public virtual PersonneMorale PersonneMorale { get; set; }
        #endregion links
    }
}
