﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Table("EMP_EMPLOYE_FONCTION", Schema = "rp")]
    public class EmployeFonction : HistoryEntity
    {
        public const String COLUMN_PREFIX = "EFO";
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX + "_ID")]
        public long EmployeFonctionID { get; set; }
        
        #region links
        [Required]
        [StringLength(10)]
        [Column(COLUMN_PREFIX + "_FK_"+Organisation.COLUMN_PREFIX+"_CODE")]
        [ForeignKey("Organisation")]
        public string OrganisationCode { get; set; }

        [Required]
        [StringLength(10)]
        [Column(COLUMN_PREFIX + "_FK_"+Fonction.COLUMN_PREFIX+"_CODE")]
        [ForeignKey("Fonction")]
        public string FonctionCode { get; set; }
        [Column(COLUMN_PREFIX + "_FK_"+Employe.COLUMN_PREFIX+"_ID")]
        [ForeignKey("Employe")]
        public long EmployeID { get; set; }

        public virtual Employe Employe { get; set; }

        public virtual Fonction Fonction { get; set; }

        public virtual Organisation Organisation { get; set; }
        #endregion links
    }
}
