﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("EMP_FONCTION", Schema = "rp")]
    public class Fonction :BaseEntity
    {
        public const String COLUMN_PREFIX = "FON";
        [Key]
        [StringLength(10)]
        [Column(COLUMN_PREFIX + "_CODE")]
        public string FonctionCode { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT")]
        public string Etat { get; set; }


        public virtual ICollection<EmployeFonction> EmployeFonctions { get; set; }
    }
}
