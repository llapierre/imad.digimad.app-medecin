﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("EMP_EMPLOYE", Schema = "rp")]
    public class Employe : HistoryEntity
    {
        public const String COLUMN_PREFIX = "EMP";
        public Employe()
        {
            EmployeFonctions = new HashSet<EmployeFonction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX + "_ID")]
        public long EmployeID { get; set; }
                        
        [Required]
        [StringLength(10)]
        [Column(COLUMN_PREFIX + "_MATRICULE")]
        public string Matricule { get; set; }

        [Column(COLUMN_PREFIX + "_DEBUT_INSTITUTION")]
        public DateTime? DebutInstitution { get; set; }

        [Column(COLUMN_PREFIX + "_TAUX_ACTIVITE")]
        public decimal? TauxActivite { get; set; }

        [Column(COLUMN_PREFIX + "_STATUS")]
        [StringLength(2)]
        public string Status { get; set; }

        [Column(COLUMN_PREFIX + "_VRH_CRC")]
        public long? VRHCRC { get; set; }

        #region links
        public virtual ICollection<EmployeFonction> EmployeFonctions { get; set; }
        [Column(COLUMN_PREFIX + "_FK_"+Personne.COLUMN_PREFIX+"_ID")]
        [ForeignKey("Personne")]
        public long PersonneID { get; set; }

        public virtual Personne Personne { get; set; }
        #endregion links
    }
}
