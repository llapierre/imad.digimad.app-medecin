﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("PER_PERSONNE", Schema = "rp")]
    public class Personne : HistoryEntity
    {
        public const String COLUMN_PREFIX = "PRS";
        public Personne()
        {
            ClientPatients = new HashSet<ClientPatient>();
            ClientProcheAidants = new HashSet<ClientProcheAidant>();
            Employes = new HashSet<Employe>();
            PartenaireMedecins = new HashSet<PartenaireMedecin>();
            PartenairePharmaciens = new HashSet<PartenairePharmacien>();
            PersonneAdresses = new HashSet<PersonneAdresse>();
            Contacts = new HashSet<Contact>();
        }
        #region fields
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long PersonneID { get; set; }
                
        [Required]
        [StringLength(50)]
        [Column(COLUMN_PREFIX + "_PRENOM")]
        public string Prenom { get; set; }

        [Required]
        [StringLength(50)]
        [Column(COLUMN_PREFIX + "_NOM")]
        public string Nom { get; set; }

        [StringLength(50)]
        [Column(COLUMN_PREFIX + "_NOM_JEUNE_FILLE")]
        public string NomJeuneFille { get; set; }

        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_SEXE")]
        public string Sexe { get; set; }

        [Column(COLUMN_PREFIX + "_DATE_NAISSANCE")]
        public DateTime? DateNaissance { get; set; }

        [Column(COLUMN_PREFIX + "_DATE_DECES")]
        public DateTime? DateDeces { get; set; }
                
        [StringLength(13)]
        [Column(COLUMN_PREFIX + "_AVS")]
        public string AVS { get; set; }
                
        [StringLength(500)]
        [Column(COLUMN_PREFIX + "_COMMENT")]
        public string Comment { get; set; }
        #endregion fields

        #region links
        [StringLength(20)]
        [Column(COLUMN_PREFIX + "_FK_"+PersonneTitre.COLUMN_PREFIX + "_CODE")]
        [ForeignKey("PersonneTitre")]
        public string PersonneTitreCode { get; set; }
        public virtual PersonneTitre PersonneTitre { get; set; }

        [StringLength(5)]
        [Column(COLUMN_PREFIX + "_FK_"+Language.COLUMN_PREFIX+"_LANGUAGE")]
        [ForeignKey("Language")]
        public string LanguageCode { get; set; }
        public virtual Language Language { get; set; }

        [StringLength(20)]
        [Column(COLUMN_PREFIX + "_FK_"+ PersonneEtatCivil .COLUMN_PREFIX+ "_ETAT_CIVIL_CODE")]
        public string PersonneEtatCivilCode { get; set; }
        public virtual PersonneEtatCivil PersonneEtatCivil { get; set; }
        
        [StringLength(3)]
        [Column(COLUMN_PREFIX + "_FK_NATIONALITE_" + Pays.COLUMN_PREFIX+"_CODE")]
        [ForeignKey("PaysNationalite")]
        public string PaysNationaliteCode { get; set; }
        public virtual Pays PaysNationalite { get; set; }
                
        public virtual ICollection<ClientPatient> ClientPatients { get; set; }

        public virtual ICollection<ClientProcheAidant> ClientProcheAidants { get; set; }

        public virtual ICollection<Employe> Employes { get; set; }
              
        public virtual ICollection<PartenaireMedecin> PartenaireMedecins { get; set; }

        public virtual ICollection<PartenairePharmacien> PartenairePharmaciens { get; set; }

        public virtual ICollection<PersonneAdresse> PersonneAdresses { get; set; }

       public virtual ICollection<Contact> Contacts { get; set; }
        
        #endregion links
    }
}
