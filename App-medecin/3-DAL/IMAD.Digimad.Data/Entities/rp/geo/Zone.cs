﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("GEO_ZONE", Schema = "rp")]
    public class Zone : BaseEntity
    {
        public const String COLUMN_PREFIX = "ZON";
        public Zone()
        {
            Pays = new HashSet<Pays>();
            ZoneEnfants = new HashSet<Zone>();
        }

        [Key]
        [StringLength(50)]
        [Column(COLUMN_PREFIX+"_CODE")]
        public string ZoneCode { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT")]
        public string Etat { get; set; }

        [Required]
        [StringLength(50)]
        [Column(COLUMN_PREFIX + "_NOM")]
        public string Nom { get; set; }

        #region links
        [Required]
        [StringLength(20)]
        [Column(COLUMN_PREFIX+"_FK_TYPE_CODE")]
        [ForeignKey("ZoneType")]
        public string ZoneTypeCode { get; set; }
        public virtual ZoneType ZoneType { get; set; }

        [StringLength(50)]
        [Column(COLUMN_PREFIX+"_FK_PARENT_ZON_CODE")]
        [ForeignKey("ZoneParent")]
        public string ZoneParentCode { get; set; }
        public virtual Zone ZoneParent { get; set; }

        public virtual ICollection<Pays> Pays { get; set; }

        public virtual ICollection<Zone> ZoneEnfants { get; set; }
        
        #endregion links
    }
}
