﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IMAD.Digimad.Data.Attributes;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("GEO_PAYS", Schema = "rp")]
    public class Pays : BaseEntity
    {
        public const String COLUMN_PREFIX = "PAY";

        public Pays()
        {
            BureauPostals = new HashSet<BureauPostal>();
            Personnes = new HashSet<Personne>();
        }

        [Key]
        [StringLength(3)]
        [Column(COLUMN_PREFIX + "_CODE")]
        public string PaysCode { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT")]
        public string Etat { get; set; }

        #region links
        [StringLength(50)]
        [Column(COLUMN_PREFIX + "_FK_" + Zone.COLUMN_PREFIX + "_CODE")]
        [ForeignKey("Zone")]
        public string ZoneCode { get; set; }
        public virtual Zone Zone { get; set; }

        public virtual ICollection<BureauPostal> BureauPostals { get; set; }

        public virtual ICollection<Personne> Personnes { get; set; }
        #endregion links
    }
}
