﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("GEO_LANGUAGE", Schema = "rp")]
    public class Language : BaseEntity
    {
        public const String COLUMN_PREFIX = "LAN";
        public Language()
        {
            Personnes = new HashSet<Personne>();
        }

        [Key]
        [StringLength(5)]
        [Column(COLUMN_PREFIX+"_CODE")]
        public string LanguageCode { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT")]
        public string Etat { get; set; }
        
        public virtual ICollection<Personne> Personnes { get; set; }
    }
}
