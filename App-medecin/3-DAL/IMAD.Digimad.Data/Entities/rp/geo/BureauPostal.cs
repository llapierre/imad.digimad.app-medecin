﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("GEO_BUREAU_POSTAL", Schema = "rp")]
    public class BureauPostal : BaseEntity
    {
        public const String COLUMN_PREFIX = "BPO";

        public BureauPostal()
        {
            AdresseStandards = new HashSet<AdresseStandard>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX + "_ID")]
        public long BureauPostalID { get; set; }

        [Required]
        [StringLength(6)]
        [Column(COLUMN_PREFIX + "_CODE_POSTAL")]
        public string CodePostal { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT")]
        public string Etat { get; set; }

        [Required]
        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_NOM")]
        public string Nom { get; set; }

        [Required]
        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_COMMUNE")]
        public string Commune { get; set; }

        [Required]
        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_CANTON")]
        public string Canton { get; set; }

        #region links
        public virtual ICollection<AdresseStandard> AdresseStandards { get; set; }

        [Required]
        [StringLength(3)]
        [Column(COLUMN_PREFIX + "_FK_"+Pays.COLUMN_PREFIX+"_COD")]
        [ForeignKey("Pays")]
        public string PaysCode { get; set; }
        public virtual Pays Pays { get; set; }
        #endregion links
    }
}
