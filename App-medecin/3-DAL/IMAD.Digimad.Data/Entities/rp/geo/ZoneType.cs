﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("GEO_ZONE_TYPE", Schema = "rp")]
    public class ZoneType : BaseEntity
    {
        public const String COLUMN_PREFIX = "ZOT";

        public ZoneType()
        {
            Zones = new HashSet<Zone>();
            ZoneTypeEnfants = new HashSet<ZoneType>();
        }

        [Key]
        [StringLength(20)]
        [Column(COLUMN_PREFIX+"_CODE")]
        public string ZoneTypeCode { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT")]
        public string Etat { get; set; }

        #region links
        [StringLength(20)]
        [Column(COLUMN_PREFIX + "_FK_PARENT_"+ COLUMN_PREFIX + "_CODE")]
        [ForeignKey("ZoneTypeParent")]
        public string ZoneTypeParentCode { get; set; }
        public virtual ZoneType ZoneTypeParent { get; set; }

        public virtual ICollection<ZoneType> ZoneTypeEnfants { get; set; }

        public virtual ICollection<Zone> Zones { get; set; }
                        
        #endregion links
    }
}
