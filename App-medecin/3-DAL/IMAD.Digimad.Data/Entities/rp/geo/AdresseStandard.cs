﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("GEO_ADRESSE_STANDARD", Schema = "rp")]
    public class AdresseStandard :BaseEntity
    {
        public const String COLUMN_PREFIX = "ADS";

        public AdresseStandard()
        {
            PersonneAdresses = new HashSet<PersonneAdresse>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long AdresseStandardID { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT")]
        public string Etat { get; set; }

        [Required]
        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_RUE")]
        public string Rue { get; set; }

        [Column(COLUMN_PREFIX + "_FK_"+ BureauPostal.COLUMN_PREFIX+ "_ID")]
        [ForeignKey("BureauPostal")]
        public long BureauPostalID { get; set; }
        public virtual BureauPostal BureauPostal { get; set; }
                
        public virtual ICollection<PersonneAdresse> PersonneAdresses { get; set; }
    }
}
