﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("PER_PERSONNE_TITRE", Schema = "rp")]
    public class PersonneTitre : BaseEntity
    {
        public const String COLUMN_PREFIX = "PRS";
        [Key]
        [StringLength(20)]
        [Column(COLUMN_PREFIX+"_TITRE_CODE")]
        public string PersonneTitreCode { get; set; }
               
        public virtual ICollection<Personne> Personnes { get; set; }
    }
}
