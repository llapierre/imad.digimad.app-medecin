﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("PER_CONTACT", Schema = "rp")]
    public class Contact : BaseEntity
    {
        public const String COLUMN_PREFIX = "CON";

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long ContactID { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT")]
        public string Etat { get; set; }

        [Required]
        [StringLength(50)]
        [Column(COLUMN_PREFIX + "_VALEUR")]
        public string Valeur { get; set; }

        #region links
        [Required]
        [StringLength(20)]
        [Column(COLUMN_PREFIX + "_FK_TYPE_"+ContactType.COLUMN_PREFIX+"_CODE")]
        [ForeignKey("ContactType")]
        public string ContactTypeCode { get; set; }
        public virtual ContactType ContactType { get; set; }

        [Column(COLUMN_PREFIX + "FK_"+Personne.COLUMN_PREFIX+"_ID")]
        [ForeignKey("Personne")]
        public long PersonneID { get; set; }
        public virtual Personne Personne { get; set; }
        
        #endregion links
    }
}
