﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("PER_CONTACT_TYPE", Schema = "rp")]
    public class ContactType : BaseEntity
    {
        public const String COLUMN_PREFIX = "COT";

        public ContactType()
        {
            Contacts = new HashSet<Contact>();
            ContactTypeEnfants = new HashSet<ContactType>();
        }

        [Key]
        [StringLength(20)]
        [Column(COLUMN_PREFIX+"_CODE")]
        public string ContactTypeCode { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT")]
        public string Etat { get; set; }

        #region links
        [StringLength(20)]
        [Column(COLUMN_PREFIX + "_FK_PARENT_TYPE_CODE")]
        [ForeignKey("ContactTypeParent")]
        public string ContactTypeParentCode { get; set; }
        public virtual ContactType ContactTypeParent { get; set; }
        
        public virtual ICollection<Contact> Contacts { get; set; }
        
        public virtual ICollection<ContactType> ContactTypeEnfants { get; set; }

        #endregion links
    }
}
