﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IMAD.Digimad.Data.Attributes;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("PER_PERSONNE_ETAT_CIVIL", Schema = "rp")]
    public class PersonneEtatCivil : BaseEntity
    {
        public const String COLUMN_PREFIX = "PRS";
        public PersonneEtatCivil()
        {
            Personnes = new HashSet<Personne>();
        }

        [Key]
        [StringLength(20)]
        [Column(COLUMN_PREFIX + "_ETAT_CIVIL_CODE")]
        public string PersonneEtatCivilCode { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX + "_ETAT_CIVIL_ETAT")]
        public string Etat { get; set; }

        #region links
        public virtual ICollection<Personne> Personnes { get; set; }
        #endregion links
    }
}
