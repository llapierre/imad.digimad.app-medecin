﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Table("PER_ADRESSE_PERSONNE", Schema = "rp")]
    public class PersonneAdresse : BaseEntity
    {
        public const String COLUMN_PREFIX = "ADP";

        #region fields
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long PersonneAdresseID { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX+"_ETAT")]
        public string Etat { get; set; }

        [Required]
        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_RUE1")]
        public string Rue1 { get; set; }

        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_RUE2")]
        public string Rue2 { get; set; }

        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_RUE3")]
        public string Rue3 { get; set; }

        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_APRES_COMMUNE")]
        public string ApresCommune { get; set; }

        [Required]
        [StringLength(6)]
        [Column(COLUMN_PREFIX + "_CODE_POSTAL")]
        public string CodePostal { get; set; }

        [Required]
        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_COMMUNE")]
        public string Commune { get; set; }

        [Required]
        [StringLength(100)]
        [Column(COLUMN_PREFIX + "_CANTON")]
        public string Canton { get; set; }

        [Required]
        [StringLength(50)]
        [Column(COLUMN_PREFIX + "_PAYS")]
        public string Pays { get; set; }
        #endregion fields

        #region links
        [Required]
        [StringLength(10)]
        [Column("ADP_FK_TYPE_CODE")]
        [ForeignKey("PersonneAdresseType")]
        public string PersonneAdresseTypeCode { get; set; }
        public virtual PersonneAdresseType PersonneAdresseType { get; set; }

        [Column(COLUMN_PREFIX+"_FK_"+ AdresseStandard.COLUMN_PREFIX+ "_ID")]
        [ForeignKey("AdresseStandard")]
        public long? AdresseStandardID { get; set; }
        public virtual AdresseStandard AdresseStandard { get; set; }

        [Column(COLUMN_PREFIX + "_FK_" + Personne.COLUMN_PREFIX + "_ID")]
        [ForeignKey("Personne")]
        public long PersonneID { get; set; }  
        public virtual Personne Personne { get; set; }        
        #endregion links
    }
}
