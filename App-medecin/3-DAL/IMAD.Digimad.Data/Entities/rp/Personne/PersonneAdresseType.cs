﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("PER_ADRESSE_PERSONNE_TYPE", Schema = "rp")]
    public class PersonneAdresseType : BaseEntity
    {
        public const String COLUMN_PREFIX = "ADP";
        public PersonneAdresseType()
        {
            PersonneAdresses = new HashSet<PersonneAdresse>();
        }

        [Key]
        [StringLength(10)]
        [Column(COLUMN_PREFIX+"_TYPE_CODE")]
        public string PersonneAdresseTypeCode { get; set; }
                
        public virtual ICollection<PersonneAdresse> PersonneAdresses { get; set; }
    }
}
