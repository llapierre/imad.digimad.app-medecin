﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("CLI_PATIENT", Schema = "rp")]
    public class ClientPatient : HistoryEntity
    {
        public const String COLUMN_PREFIX = "RPE";

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long ClientPatientID { get; set; }
         
        #region links
        [Column(COLUMN_PREFIX + "_FK_"+Personne.COLUMN_PREFIX+"_ID")]
        [ForeignKey("Personne")]
        public long Personne_PersonneID { get; set; }
        public virtual Personne Personne { get; set; }

        [StringLength(10)]
        [Column(COLUMN_PREFIX + "_FK_"+Organisation.COLUMN_PREFIX+"_CODE")]
        [ForeignKey("Organisation")]
        public string OrganisationCode { get; set; }

        public virtual Organisation Organisation { get; set; }

        
        #endregion links
    }
}
