﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("CLI_PROCHE_AIDANT", Schema = "rp")]
    public class ClientProcheAidant : HistoryEntity
    {
        public const String COLUMN_PREFIX = "RPE";

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long ClientProcheAidantID { get; set; }

        #region links
        [Column(COLUMN_PREFIX+"_FK_"+Personne.COLUMN_PREFIX+"_ID")]
        [ForeignKey("Personne")]
        public long PersonneID { get; set; }

        public virtual Personne Personne { get; set; }
        #endregion links
    }
}
