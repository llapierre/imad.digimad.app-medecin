﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("ORG_ORGANISATION_TYPE", Schema = "rp")]
    public class OrganisationType : HistoryEntity
    {
        public const String COLUMN_PREFIX = "ORG";
        public OrganisationType()
        {
            Organisations = new HashSet<Organisation>();
        }

        [Key]
        [StringLength(10)]
        [Column(COLUMN_PREFIX + "_TYPE_CODE")]
        public string OrganisationTypeCode { get; set; }
        
        #region links
        public virtual ICollection<Organisation> Organisations { get; set; }
        #endregion links
    }
}
