﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Table("ORG_ORGANISATION_HIERARCHY", Schema = "rp")]
    public class OrganisationHierarchy : HistoryEntity
    {
        public const String COLUMN_PREFIX = "ORH";
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long OrganisationHierarchyID { get; set; }
        
        #region links
        [StringLength(10)]
        [Column(COLUMN_PREFIX + "_FK_PARENT_"+COLUMN_PREFIX+"_ID")]
        [ForeignKey("OrganisationParent")]
        public string OrganisationParentID { get; set; }

        [Required]
        [StringLength(10)]
        [Column(COLUMN_PREFIX + "_FK_ENFANT_"+COLUMN_PREFIX+"_ID")]
        [ForeignKey("OrganisationEnfant")]
        public string OrganisationEnfantID { get; set; }

        public virtual Organisation OrganisationParent { get; set; }

        public virtual Organisation OrganisationEnfant { get; set; }
        #endregion links
    }
}
