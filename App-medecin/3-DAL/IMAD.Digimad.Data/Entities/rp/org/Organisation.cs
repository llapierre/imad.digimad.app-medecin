﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("string")]
    [Table("ORG_ORGANISATION", Schema = "rp")]
    public class Organisation : HistoryEntity
    {
        public const String COLUMN_PREFIX = "ORG";
        public Organisation()
        {
            ClientPatients = new HashSet<ClientPatient>();
            EmployeFonctions = new HashSet<EmployeFonction>();
            OrganisationParents = new HashSet<OrganisationHierarchy>();
            OrganisationEnfants = new HashSet<OrganisationHierarchy>();
        }

        [Key]
        [StringLength(10)]
        [Column(COLUMN_PREFIX+"_CODE")]
        public string OrganisationCode { get; set; }
                
        [Column(COLUMN_PREFIX + "_DISCRIMINANT")]
        [Required]
        [StringLength(3)]
        public string Discriminant { get; set; }

        [Column(COLUMN_PREFIX + "_NOM")]
        [Required]
        [StringLength(100)]
        public string Nom { get; set; }

        [Column(COLUMN_PREFIX + "_FK_"+OrganisationType.COLUMN_PREFIX+"_TYPE_CODE")]
        [Required]
        [StringLength(10)]
        [ForeignKey("OrganisationType")]
        public string OrganisationTypeCode { get; set; }

        #region links
        public virtual ICollection<ClientPatient> ClientPatients { get; set; }

        public virtual ICollection<EmployeFonction> EmployeFonctions { get; set; }

        public virtual OrganisationType OrganisationType { get; set; }

        public virtual ICollection<OrganisationHierarchy> OrganisationParents { get; set; }

        public virtual ICollection<OrganisationHierarchy> OrganisationEnfants { get; set; }
        #endregion links
    }
}
