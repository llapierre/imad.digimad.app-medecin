﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("ORG_PERSONNE_MORALE", Schema = "rp")]
    public class PersonneMorale : HistoryEntity
    {
        public const String COLUMN_PREFIX = "PMO";
        public PersonneMorale()
        {
            PartenaireMedecins = new HashSet<PartenaireMedecin>();
            PartenairePharmaciens = new HashSet<PartenairePharmacien>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long PersonneMoraleID { get; set; }
             
        [Required]
        [StringLength(80)]
        [Column(COLUMN_PREFIX+"_LIBELLE")]
        public string Libelle { get; set; }

        #region links
        public virtual ICollection<PartenaireMedecin> PartenaireMedecins { get; set; }

        public virtual ICollection<PartenairePharmacien> PartenairePharmaciens { get; set; }
        #endregion links
    }
}
