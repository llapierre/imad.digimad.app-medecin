﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("PRE_REFERANT", Schema = "rp")]
    public class Referant : HistoryEntity
    {
        public const String COLUMN_PREFIX = "REF";

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long ReferantID { get; set; }

        #region links
        [Column(COLUMN_PREFIX+"_FK_"+Mandat.COLUMN_PREFIX+"_ID")]
        [ForeignKey("Mandat")]
        public long MandatID { get; set; }
        public virtual Mandat Mandat { get; set; }

        [Required]
        [StringLength(20)]
        [Column(COLUMN_PREFIX+"_FK_ROLE_CODE")]
        [ForeignKey("ReferantRole")]
        public string ReferantRoleCode { get; set; }
        public virtual ReferantRole ReferantRole { get; set; }

        [Column(COLUMN_PREFIX + "_FK_"+PartenaireMedecin.COLUMN_PREFIX+"_ID")]
        public long PartenaireMedecinID { get; set; }
        
        #endregion links
    }
}
