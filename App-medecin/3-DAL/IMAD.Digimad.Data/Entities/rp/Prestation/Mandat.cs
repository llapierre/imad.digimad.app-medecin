﻿using IMAD.Digimad.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Repository("long")]
    [Table("PRE_MANDAT", Schema = "rp")]
    public class Mandat : HistoryEntity
    {
        public const String COLUMN_PREFIX = "MAN";
        public Mandat()
        {
            Referants = new HashSet<Referant>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(COLUMN_PREFIX+"_ID")]
        public long MandatID { get; set; }
        
        [Required]
        [StringLength(3)]
        [Column(COLUMN_PREFIX + "_TYPE")]
        public string Type { get; set; }

        #region links
        [Column(COLUMN_PREFIX + "_FK_"+PartenaireMedecin.COLUMN_PREFIX+"_ID")]
        public long? PartenaireMedecinID { get; set; }
        
        public virtual ICollection<Referant> Referants { get; set; }

        [Column(COLUMN_PREFIX + "_FK_PATIENT_"+ClientPatient.COLUMN_PREFIX+"_ID")]
        public long? ClientPatientID { get; set; }

        [Column(COLUMN_PREFIX + "_FK_PROCHE_AIDANT_"+ClientProcheAidant.COLUMN_PREFIX+"_ID")]
        public long? ClientProcheAidantID { get; set; }
              
        #endregion links
    }
}
