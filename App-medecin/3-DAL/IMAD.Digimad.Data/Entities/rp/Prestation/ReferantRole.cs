﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    [Table("PRE_ROLE_REFERANT", Schema = "rp")]
    public class ReferantRole : BaseEntity
    {
        public const String COLUMN_PREFIX = "REF";
        public ReferantRole()
        {
            Referants = new HashSet<Referant>();
        }

        [Key]
        [StringLength(20)]
        [Column(COLUMN_PREFIX+"_ROLE_CODE")]
        public string ReferantRoleCode { get; set; }

        [Required]
        [StringLength(1)]
        [Column(COLUMN_PREFIX+"_ROLE_ETAT")]
        public string Etat { get; set; }

        #region links
        public virtual ICollection<Referant> Referants { get; set; }
        #endregion links
    }
}
