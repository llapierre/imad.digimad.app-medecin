﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Entities
{
    public class HistoryEntity : BaseEntity
    {
        public const String COLUMN_NAME_ETAT = "ETAT";
        public const String COLUMN_NAME_DATEDEBUT = "DATE_DEBUT";
        public const String COLUMN_NAME_DATEFIN = "DATE_FIN";
        [Required]
        [StringLength(1)]
        [Column(TypeName = "char")]
        public string Etat { get; set; }
        
        public DateTime? DateDebut { get; set; }
               
        public DateTime? DateFin { get; set; }

    }
}
