﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class RepositoryAttribute : Attribute
    {
        public RepositoryAttribute(string keyType)
        {
            this.keyType = keyType;
        }

        private string keyType;

        public string KeyType { get { return keyType; } }
    }
}
