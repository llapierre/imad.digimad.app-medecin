﻿using IMAD.Digimad.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Repositories
{
    public abstract class RepositoryBase<TEntity, Tkey> : IRepository<TEntity, Tkey>, IDisposable where TEntity : class
    {
        protected readonly DbSet<TEntity> dbSet;
        public IUnitOfWork UnitOfWork { get; set; }

        public RepositoryBase()
        {

        }
        public RepositoryBase(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
            dbSet = UnitOfWork.Context.Set<TEntity>();
        }

        public IQueryable<TEntity> Fetch()
        {
            return dbSet;
        }

        public string ConnectionString { get { return UnitOfWork.Context.Database.Connection.ConnectionString; } }

        public void Attach(TEntity entity)
        {
            dbSet.Attach(entity);
        }

        public TEntity Get(Tkey id)
        {
            return dbSet.Find(id);
        }

        public void Create(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            UnitOfWork.Context.Entry(entity).State = EntityState.Modified;
        }
        public void Reload(TEntity entity)
        {
            UnitOfWork.Context.Entry(entity).Reload();
        }

        public void Delete(TEntity entity)
        {
            dbSet.Remove(entity);
        }

        public void Delete(Tkey key)
        {
            TEntity entity = Get(key);
            Delete(entity);
        }

        public void RefreshDependencies(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public bool Exists(Tkey key)
        {
            return Get(key) != null;
        }

        public void Save()
        {
            UnitOfWork.Save();
        }

        public void Dispose()
        {
            UnitOfWork.Dispose();
        }
       
    }
}
