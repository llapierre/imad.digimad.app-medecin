﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMAD.Digimad.Data.Repositories
{
    public interface IRepository<TEntity, TKey> : IDisposable where TEntity : class
    {
        void Create(TEntity entity);
        void Delete(TEntity entity);
        void Delete(TKey key);        
        bool Exists(TKey key);
        IQueryable<TEntity> Fetch();
        TEntity Get(TKey id);
        void RefreshDependencies(TEntity entity);
        void Save();
        void Update(TEntity entity);
    }
}
