/*
 * SQL SErver: replace TIMSTAMP( by CONVERT(datetime, 
 */

--INSERT INTO "RP.SEC_APPLICATION" (APP_CODE) VALUES
--	('medlink'),
--	('app-medecin'),
--	('app-client');

INSERT INTO rp.GEO_LANGUAGE (LAN_CODE,LAN_ETAT) VALUES ('fr_ch','1');

INSERT INTO rp.GEO_PAYS (PAY_CODE,PAY_FK_ZON_CODE,PAY_ETAT) VALUES ('CH', null,'1'), ('FR', null,'1'), ('IT', null,'1'), ('DE', null,'1');

INSERT INTO RP.ORG_ORGANISATION_TYPE
	(ORG_TYPE_CODE,ORG_ETAT) VALUES
	('DIRGE','1'),
	('DIR','1'),
	('SER','1'),
	('EQU','1');

INSERT INTO RP.ORG_ORGANISATION
	(ORG_CODE, ORG_DISCRIMINANT, ORG_NOM, ORG_FK_ORG_TYPE_CODE, ORG_ETAT) VALUES
	('DIRGE', 'OFO', 'Direction Générale', 'DIRGE','1'),
	('DEX', 'OFO', 'Direction Exploitation', 'DIR','1'),
	('DSI', 'OFO', 'Direction Des Systèmes d information', 'DIR','1'),
	('MOB', 'OFO', 'Service mobilité', 'SER','1'),
	('DPR', 'OFO', 'Direction Des Prestations', 'DIR','1'),
	('CMD1', 'OOP', 'CMD Carrouge', 'EQU','1'),
	('EQU1', 'OOP', 'Equipe 1', 'EQU','1'),
	('EQU2', 'OOP', 'Equipe 2', 'EQU','1'),
	('CMD2', 'OOP', 'CMD Onex', 'EQU','1');

INSERT INTO RP.ORG_ORGANISATION_HIERARCHY (ORH_ID, ORH_FK_PARENT_ORH_ID, ORH_FK_ENFANT_ORH_ID, ORH_ETAT) VALUES
		(1, 'DEX',  'DIRGE','1'),
		(2, 'DSI',  'DEX','1'),
		(3, 'MOB',  'DSI','1'),
		(4, 'DPR',  'DIRGE','1'),
		(5, 'CMD1', 'DPR','1'),
		(6, 'EQU1', 'CMD1','1'),
		(7, 'EQU2', 'CMD1','1'),
		(8, 'CMD2', 'DPR','1');

INSERT INTO rp.EMP_FONCTION (FON_CODE,FON_ETAT) VALUES ('INF','1'), ('AD','1'), ('RE','1'), ('ASA','1'), ('ASSC','1');

INSERT INTO rp.PER_PERSONNE
	(PRS_ID, PRS_PRENOM,PRS_NOM,PRS_SEXE,PRS_DATE_NAISSANCE,PRS_AVS,PRS_COMMENT,PRS_FK_PRS_CODE,PRS_FK_LAN_LANGUAGE,PRS_FK_PRS_ETAT_CIVIL_CODE,PRS_FK_NATIONALITE_PAY_CODE, Etat) VALUES
	(1, 'Paul', 'Lemed',  'H', CONVERT(datetime,'1962-09-23 03:23:34.234'), '1234567890123', 'Medecin pour les tests', 'Docteur', 'fr_ch', 'MARIE', 'CH','1'),
	(2, 'Jeanne', 'Lapicouse',  'F', CONVERT(datetime,'1970-02-12 02:01:12.222'), '0987654321321', 'Infirmière', 'Madame', 'fr_ch', 'CELIBATAIRE', 'CH','1'),
	(3, 'Bob', 'Linconnu',  'H', CONVERT(datetime,'1970-02-12 02:01:12.222'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'CH','1'),
	(4, 'Rick', 'Limad',  'H', CONVERT(datetime,'1970-02-12 02:01:12.222'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'CH','1'),
	(5, 'Thibault', 'Cuvillier', 'H', CONVERT(datetime,'1962-08-09 01:01:01.001'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'FR','1'),
	(6, 'Karim', 'Smartwave', 'H', CONVERT(datetime,'1970-08-09 01:01:01.001'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'FR','1'),
	(7, 'Klaus', 'Dierk', 'H', CONVERT(datetime,'1970-08-09 01:01:01.001'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'FR','1'),
	(8, 'Robert', 'Patient1', 'H', CONVERT(datetime,'1970-08-09 01:01:01.001'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'FR','1'),
	(9, 'Maurice', 'Patient2', 'H', CONVERT(datetime,'1970-08-09 01:01:01.001'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'FR','1'),
	(10, 'Leon', 'Patient3', 'H', CONVERT(datetime,'1970-08-09 01:01:01.001'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'FR','1'),
	(11, 'Chritopher', 'Patient4', 'H', CONVERT(datetime,'1970-08-09 01:01:01.001'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'FR','1'),
	(12, 'Maurice', 'Patient5', 'H', CONVERT(datetime,'1970-08-09 01:01:01.001'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'FR','1'),
	(13, 'Enrico', 'Patient6', 'H', CONVERT(datetime,'1970-08-09 01:01:01.001'), NULL, NULL, 'Monsieur', 'fr_ch', NULL, 'FR','1');

/*
 * Les patients
 */	
INSERT INTO RP.CLI_PATIENT (RPE_ID,RPE_FK_PRS_ID, RPE_FK_ORG_CODE, RPE_ETAT) VALUES
             (8 ,8, 'EQU1','1')
           , (9 ,9, 'EQU1','1')
           , (10 ,10, 'EQU1','1')
           , (11 ,11, 'EQU2','1')
           , (12 ,12, null,'1')
           , (13 ,13, 'EQU2','1');

INSERT INTO RP.PRE_MANDAT
           (MAN_ID,MAN_TYPE,MAN_FK_RPE_ID,MAN_ETAT)
     VALUES	 (8, 'MPT',8,'1'),
		     (9, 'MPT',9,'1'),
		     (10, 'MPT',10,'1'),
		     (11, 'MPT',11,'1'),
		     (12, 'MPT',12,'1'),
		     (13, 'MPT',13,'1');

INSERT INTO RP.PRE_REFERANT (REF_ID, REF_FK_MAN_ID, REF_FK_ROLE_CODE, REF_FK_RPE_ID,REF_ETAT)
	VALUES
		(8, 8, 'MED-REFERANT', 1,'1'),
		(9, 9, 'MED-REFERANT', 1,'1'),
		(10, 10, 'MED-REFERANT', 1,'1'),
		(11, 11, 'MED-REFERANT', 1,'1'),
		(12, 12, 'MED-REFERANT', 1,'1'),
		(13, 13, 'MED-REFERANT', 1,'1');
		
INSERT INTO rp.PER_ADRESSE_PERSONNE (
		ADP_ID,
		ADP_RUE1,
		ADP_CODE_POSTAL,
		ADP_COMMUNE,
		ADP_CANTON,
		ADP_PAYS,
		ADP_FK_TYPE_CODE,
		ADP_FK_ADS_ID,
		ADP_FK_PRS_ID,
		ADP_ETAT
	) VALUES (
		13,
		'route du Molard 122',
		'1090',
		'La croix sur Lutry',
		'VD',
		'CH',
		'DOMICILE',
		NULL,
		13,
		'1'
	);
		
/*
 * Le medecin avec une swissID
 */
INSERT INTO rp.ORG_PERSONNE_MORALE (PMO_ID,PMO_LIBELLE,PMO_ETAT) VALUES (1, 'Médecin Paul Lemed','1');
INSERT INTO rp.PAR_MEDECIN (RPE_ID, RPE_FK_PRS_ID,RPE_FK_PMO_ID,RPE_ETAT) VALUES (1,1,1,'1');
--INSERT INTO rp.SEC_LOGIN (LOG_ID,LOG_FK_LTY_CODE,LOG_FK_PRS_ID) VALUES (1, 'SWISSID', 1);
--INSERT INTO rp.SEC_LOGIN_PROPERTY (LOP_KEY,LOP_FK_LOG_ID,LOP_VALUE) VALUES ('SWISSID.ID', 1, '11111-1111-1111-1111');

/*
 * L'infirmière
 */
INSERT INTO rp.EMP_EMPLOYE (EMP_ID,EMP_FK_PRS_ID,EMP_MATRICULE,EMP_DEBUT_INSTITUTION,EMP_TAUX_ACTIVITE,EMP_STATUS,EMP_ETAT)
VALUES (2,2, '103250', CONVERT(datetime,'2014-09-23 03:23:34.234'), 100, 'HP','1');
INSERT INTO RP.EMP_EMPLOYE_FONCTION (EFO_ID, EFO_FK_ORG_CODE, EFO_FK_FON_CODE, EFO_FK_EMP_ID,EFO_ETAT) VALUES (1, 'EQU1', 'INF', 2,'1');
--INSERT INTO RP.SEC_LOGIN (LOG_ID,LOG_FK_LTY_CODE,LOG_FK_PRS_ID) VALUES (2, 'SWISSID', 2);
--INSERT INTO RP.SEC_LOGIN_PROPERTY (LOP_KEY,LOP_FK_LOG_ID,LOP_VALUE) VALUES  ('SWISSID.ID', 2, '22222-2222-2222-2222');

/*
 * Employe Imad sans fonction
 */
INSERT INTO RP.EMP_EMPLOYE (EMP_ID,EMP_FK_PRS_ID,EMP_MATRICULE,EMP_DEBUT_INSTITUTION,EMP_TAUX_ACTIVITE,EMP_STATUS, EMP_ETAT)
VALUES (4, 4,'103250', CONVERT(datetime,'2014-09-23 03:23:34.234'), 100, 'HP','1');

/*
 * Thibault
 */
INSERT INTO RP.EMP_EMPLOYE (EMP_ID,EMP_FK_PRS_ID,EMP_MATRICULE,EMP_DEBUT_INSTITUTION,EMP_TAUX_ACTIVITE,EMP_STATUS,EMP_ETAT)
	VALUES (5, 5, '113564', CONVERT(datetime,'2014-09-23 03:23:34.234'), 100, 'HP','1');


--INSERT INTO "RP.SEC_LOGIN" (LOG_ID,LOG_FK_LTY_CODE,LOG_FK_PRS_ID) VALUES (5, 'SWISSID', 5);
--INSERT INTO "RP.SEC_LOGIN_PROPERTY" (LOP_KEY,LOP_FK_LOG_ID,LOP_VALUE) VALUES ('SWISSID.ID', 5, '1300-8015-6755-6978');

INSERT INTO RP.PER_ADRESSE_PERSONNE (
		ADP_ID,
		ADP_RUE1,
		ADP_CODE_POSTAL,
		ADP_COMMUNE,
		ADP_CANTON,
		ADP_PAYS,
		ADP_FK_TYPE_CODE,
		ADP_FK_ADS_ID,
		ADP_FK_PRS_ID,
		ADP_ETAT
	) VALUES (
		5,
		'route du Molard 233',
		'1090',
		'La croix sur Lutry',
		'VD',
		'CH',
		'DOMICILE',
		NULL,
		5
		,'1'
	);

/*
 * Karim
 */
--INSERT INTO "RP.SEC_LOGIN" (LOG_ID,LOG_FK_LTY_CODE,LOG_FK_PRS_ID) VALUES (6, 'SWISSID', 6);
--INSERT INTO "RP.SEC_LOGIN_PROPERTY" (LOP_KEY,LOP_FK_LOG_ID,LOP_VALUE) VALUES ('SWISSID.ID', 6, '1300-8015-0702-9730');

/*
 * Klaus
 */
INSERT INTO RP.EMP_EMPLOYE (EMP_ID,EMP_FK_PRS_ID,EMP_MATRICULE,EMP_DEBUT_INSTITUTION,EMP_TAUX_ACTIVITE,EMP_STATUS,EMP_ETAT)
	VALUES (7, 7, '113564', CONVERT(datetime,'2014-09-23 03:23:34.234'), 100, 'HP','1');

--INSERT INTO "RP.SEC_LOGIN" (LOG_ID,LOG_FK_LTY_CODE,LOG_FK_PRS_ID) VALUES (7, 'SWISSID', 7);
--INSERT INTO "RP.SEC_LOGIN_PROPERTY" (LOP_KEY,LOP_FK_LOG_ID,LOP_VALUE) VALUES ('SWISSID.ID', 7, '1300-8015-6892-95880');


/*
 * AUTHORISATIONS
 */
--INSERT INTO RP.SEC_AUTHORISABLE (ATS_ID, ATS_TYPE, AAP_FK_APP_CODE) VALUES
--	(1, 'AAP', 'medlink'),
--	(2, 'AAP', 'app-medecin'),
--	(3, 'AAP', 'app-client');

--INSERT INTO RP.SEC_ROLE (ROL_CODE) VALUES
--	('/EMP'),
--	('/EMP/INF'),
--	('/EMP/ASSC'),
--	('/EMP/ASA'),
--	('/PAR'),
--	('/PAR/MED'),
--	('/PAR/PHA'),
--	('/CLI/PAT'),
--	('/CLI/PRA');

--INSERT INTO RP.SEC_AUTHORISATION ( AUT_ID,AUT_TYPE,AUT_FK_RUL_CODE,AUT_FK_ATS_ID,RAU_FK_ROL_CODE,PAU_FK_PRS_ID) VALUES
--	(1, 'RAU', 'ALLOWED', 1, '/EMP/INF', NULL),
--	(2, 'RAU', 'ALLOWED', 3, '/PAR/MED', NULL),
--	(3, 'PAU', 'ALLOWED', 2, NULL, 3),
--	(4, 'PAU', 'ALLOWED', 1, NULL, 4),
--	(5, 'PAU', 'ALLOWED', 1, NULL, 5),
--	(6, 'PAU', 'ALLOWED', 2, NULL, 5),
--	(7, 'PAU', 'ALLOWED', 3, NULL, 6),
--	(8, 'PAU', 'ALLOWED', 2, NULL, 7),
--	(9, 'PAU', 'ALLOWED', 1, NULL, 7);
